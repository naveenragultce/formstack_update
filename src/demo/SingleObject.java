package demo;

public class SingleObject{
	
	  //create an object of SingleObject
	   private static SingleObject instance = new SingleObject();

	   //make the constructor private so that this class cannot be
	   //instantiated
	//  private SingleObject(){}

	   //Get the only object available
	   public static SingleObject getInstance(){
	      return instance;
	   }

	   public void connect(){
	      System.out.println("connected to singelton ");
	   }
	   public void disconnect(){
		      System.out.println("Disconnected from singelton");
		   }
	}




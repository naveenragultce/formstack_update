package demo;

import java.awt.AWTException;
import java.awt.List;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import java.util.*;

public class formstack {

	

	@Test
	public void Login() throws InterruptedException, AWTException 
{
		
		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.formstack.com/ ");
		driver.manage().window().maximize();
	WebElement login=driver.findElement(By.xpath("(//div//ul//a[@href='https://www.formstack.com/admin/'])[2]"));	
	login.click();
	WebElement email=driver.findElement(By.xpath("//*[@id='email']"));	
	email.sendKeys("Naveenragultce@gmail.com");
	WebElement password=driver.findElement(By.xpath("//*[@id='password']"));
	password.sendKeys("Nady09nady09.");
	WebElement lsubmit=driver.findElement(By.xpath("//*[@id='submit']"));	
	lsubmit.click();
	Thread.sleep(2000);
	JavascriptExecutor js = (JavascriptExecutor) driver;
	js.executeScript("window.scrollBy(0,500)");
	
	WebElement createtemp=driver.findElement(By.xpath("(//*[@class='fs-btn2 fs-btn2--style_create fs-btn2--size_large'])[2]"));	
	createtemp.click();
	Thread.sleep(1000);
	WebElement form=driver.findElement(By.xpath("//*[@class='FT_b8709 FT_ec648']"));	
	form.click();
	WebElement next=driver.findElement(By.xpath("//*[@class=' phx-Button phx-Button--medium phx-Button--link phx-ActionButton']"));	
	next.click();
	
	WebDriverWait wait=new WebDriverWait(driver, 10);
	
	String firstName = generateRandomName(5);
	WebElement formname = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@type='text' and @name='formName']")));
	formname.sendKeys(firstName);
	
	
			WebElement formurl=driver.findElement(By.xpath("//*[@type='text' and @name='formURL']"));	
	formurl.sendKeys(firstName);
	
	WebElement startwithtemp=driver.findElement(By.xpath("//*[@class=' phx-Button phx-Button--medium phx-Button--link phx-ActionButton']"));	
	startwithtemp.click();
	
	java.util.List<WebElement> list=driver.findElements(By.xpath("//*[@class='FT_e5e71']"));
System.out.println("Size is "+list.size());

//System.out.println("name of elements are" +list.get(i).getAttribute() );	

for (WebElement webElement : list) {
    String name = webElement.getText();
    System.out.println(name);
}

WebElement nextstep=driver.findElement(By.xpath("//*[@class='phx-Button-content' and text()='Next Step']"));
nextstep.click();

WebElement theme=driver.findElement(By.xpath("//*[@style='fill: rgb(55, 64, 70); stroke: rgb(172, 181, 191);']"));
theme.click();

js.executeScript("window.scrollBy(0,500)");
//*[@class=' phx-Button phx-Button--medium phx-Button--link phx-ActionButton']

WebElement finish=driver.findElement(By.xpath("//*[@class=' phx-Button phx-Button--medium phx-Button--link phx-ActionButton']"));
finish.click();

// Drag and Drop

Thread.sleep(5000);
//WebElement from =wait.until(ExpectedConditions.visibilityOfElementLocated((By.xpath("(//*[@class='FI_a2a10 FSSBI_e9797 DH_97662'])[1]"))));
//from.click();
//WebElement to =wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//*[@class='BPF_c0b57 BPCB_f0adf DH_97662'])[1]")));
WebElement drag=driver.findElement(By.xpath("(//*[@draggable='true'])[1]"));
String dragtext=drag.getText();
Actions action=new Actions(driver);
action.doubleClick(drag).perform();
Thread.sleep(5000);
java.util.List<WebElement> elelist=driver.findElements(By.xpath("//*[@class='BPF_fedaf']"));
System.out.println(elelist.size());
for (WebElement nameelelist : elelist) {
    String name = nameelelist.getText();
  //  System.out.println(name);
    if(name.equalsIgnoreCase(dragtext))
    {
    	int i=1;
    	System.out.println("Success: Added web element is present in the list for" +i+ " time");
    }
    
}



Thread.sleep(1000);
WebElement live=driver.findElement(By.xpath("//*[@class='VFD_1e67f']"));
live.click();
ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles()); 
driver.switchTo().window(tabs.get(1));
Thread.sleep(1000);
String url= driver.getCurrentUrl();
System.out.println("Current url is " +url);
//WebElement local=wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//*[@class='fsOptionLabel vertical'])[1]")));
//local.click();
newpage np=new  newpage();
np.current(driver, elelist);
//np.cmplist(list);
}
	
	
	
	
	
public String generateRandomName(int length) {
char[] chars ="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" .toCharArray();
StringBuilder sb = new StringBuilder();
Random random = new Random();

for (int i = 0; i < length; i++) {

	char c = chars[random.nextInt(chars.length)];
	
	sb.append(c);
	    }
	
String randomString = sb.toString();

return randomString;
	}

@Test
public void abst()
{
	formstacklive a=new formstacklive();
	a.urlcheck();
	a.intfprint();
	}
	
@BeforeSuite
public void Singleton()  
{
	SingleObject object = SingleObject.getInstance();

object.connect();

}

@AfterSuite
public void Singletondisconnect()  
{
	SingleObject object = SingleObject.getInstance();

object.disconnect();

}

}
